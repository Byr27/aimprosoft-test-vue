import Vue from 'vue';

// libs
import 'normalize.css';
import 'material-design-icons/iconfont/material-icons.css';

import App from './App';

Vue.config.devtools = true;

const app = new Vue({
  render: h => h(App),
});

app.$mount('#app');
